# contain the actual tests



def test_get(client):
    """Start with a blank database."""

    rv = client.get('/')
    
    assert rv.status == '200 OK'
    json_data = rv.get_json()
    assert json_data['message'] == 'Cloud Software and Systems'