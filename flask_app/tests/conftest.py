import pytest

import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
app_name = "app"
app_file_name = os.getenv('app_file')
if app_file_name is not None:
  app_name = app_file_name.split(".")[0]

app_module = __import__(app_name)

@pytest.fixture
def client():
    app = app_module.app
    app.config["TESTING"] = True
    with app.test_client() as client:
        yield client