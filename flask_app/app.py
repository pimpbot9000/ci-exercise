"""Simple flask app"""
from flask import Flask

app = Flask(__name__)

@app.route("/")
def get_route():
    """Hello world controller."""
    output = {'message': 'Cloud Software and Systems'}
    return output, 200
